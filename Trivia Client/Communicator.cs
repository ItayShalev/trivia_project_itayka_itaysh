﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using MessagePack;
using System.Text.Json;

namespace Trivia_Client
{
    class Communicator
    {
        static public TcpClient clientSocket;
        static private NetworkStream stream;

        static Communicator()
        {
            AppDomain.CurrentDomain.ProcessExit += CommunicatorDestructor; // Free socket on app close
            // Generate random port and bind socket
            Random random = new Random(); // Randomizer
            IPAddress ipAddress = IPAddress.Loopback; // Current Host
            IPEndPoint ipLocalEndPoint = new IPEndPoint(ipAddress, random.Next(49152, 65535)); // Bind local endpoint to random port
            clientSocket = new TcpClient(ipLocalEndPoint); // Create socket
            clientSocket.Connect("127.0.0.1", 2212); // Connect to trivia server
            if (!clientSocket.Connected)
            {
                // Exception if there was an error
                throw new SocketException();
            }
        }

        static void CommunicatorDestructor(object sender, EventArgs e)
        {
            clientSocket.Close(); // Close the socket
        }

        static public object Communicate(object request, RequestCodes requestCode)
        {
            stream = clientSocket.GetStream(); // Gets stream for easier communication
            sendMessage(stream, request, requestCode); // Sends the request using the stream
            object responseStruct = getResponseStruct(getMessage(stream)); // Get the response
            return responseStruct;
        }

        static private Tuple<int, string> getMessage(NetworkStream stream)
        {
            // Get message code
            byte[] messageCode = new byte[1];
            stream.Read(messageCode, 0, 1);

            // Get message length
            byte[] messageLength = new byte[4];
            stream.Read(messageLength, 0, 4);

            Array.Reverse(messageLength); // Fix the length
            
            // Read message according to length
            byte[] message = new byte[BitConverter.ToInt32(messageLength, 0)];
            stream.Read(message, 0, BitConverter.ToInt32(messageLength, 0));

            stream.Flush(); // Clear the stream

            // Creates a tuple with the message code and the return json
            return Tuple.Create((int)messageCode[0], MessagePackSerializer.ConvertToJson(message));
        }

        static private void sendMessage(NetworkStream stream, object request, RequestCodes requestCode)
        {
            string jsonString = "";
            // If the request has data it converts it to json
            switch(requestCode)
            {
                case RequestCodes.LOGIN:
                    jsonString = JsonSerializer.Serialize<LoginRequest>((LoginRequest)request);
                    break;
                case RequestCodes.SIGNUP:
                    jsonString = JsonSerializer.Serialize<SignupRequest>((SignupRequest)request);
                    break;
                case RequestCodes.GETPLAYERSINROOM:
                    jsonString = JsonSerializer.Serialize<GetPlayersInRoomRequest>((GetPlayersInRoomRequest)request);
                    break;
                case RequestCodes.JOINROOM:
                    jsonString = JsonSerializer.Serialize<JoinRoomRequest>((JoinRoomRequest)request);
                    break;
                case RequestCodes.CREATEROOM:
                    jsonString = JsonSerializer.Serialize<CreateRoomRequest>((CreateRoomRequest)request);
                    break;
            }

            byte[] message = new byte[0];

            if (jsonString.Length != 0)
            {
                // If there is data it converts the json into bytes
                message = MessagePackSerializer.ConvertFromJson(jsonString);
            }

            // Generates the message length
            byte[] messageLength = BitConverter.GetBytes(message.Length);
            Array.Reverse(messageLength);

            byte[] serializedMessage = new byte[5 + message.Length]; // Whole message array
            serializedMessage[0] = (byte)requestCode; // Sets the request code
            messageLength.CopyTo(serializedMessage, 1); // Inserts the data length
            if (message.Length != 0)
            {
                // If there is data it inserts it
                message.CopyTo(serializedMessage, 5);
            }

            // Sends the message to the server
            stream.Write(serializedMessage, 0, serializedMessage.Length);
        }

        static private object getResponseStruct(Tuple<int, string> messageTuple)
        {
            // Creating the response struct according to the response code
            switch (messageTuple.Item1)
            {
                case (int)ResponseCodes.ERROR:
                    Console.WriteLine("ERROR: {0}", JsonSerializer.Deserialize<ErrorResponse>(messageTuple.Item2).message);
                    System.Windows.Application.Current.Shutdown();
                    return JsonSerializer.Deserialize<ErrorResponse>(messageTuple.Item2);
                case (int)ResponseCodes.LOGIN:
                    return JsonSerializer.Deserialize<LoginResponse>(messageTuple.Item2);
                case (int)ResponseCodes.SIGNUP:
                    return JsonSerializer.Deserialize<SignupResponse>(messageTuple.Item2);
                case (int)ResponseCodes.LOGOUT:
                    return JsonSerializer.Deserialize<LogoutResponse>(messageTuple.Item2);
                case (int)ResponseCodes.GETROOMS:
                    return JsonSerializer.Deserialize<GetRoomsResponse>(messageTuple.Item2);
                case (int)ResponseCodes.GETPLAYERSINROOM:
                    return JsonSerializer.Deserialize<GetPlayersInRoomResponse>(messageTuple.Item2);
                case (int)ResponseCodes.JOINROOM:
                    return JsonSerializer.Deserialize<JoinRoomResponse>(messageTuple.Item2);
                case (int)ResponseCodes.CREATEROOM:
                    return JsonSerializer.Deserialize<CreateRoomResponse>(messageTuple.Item2);
                case (int)ResponseCodes.GETSTATS:
                    return JsonSerializer.Deserialize<GetStatisticsResponse>(messageTuple.Item2);
                case (int)ResponseCodes.CLOSEROOM:
                    return JsonSerializer.Deserialize<CloseRoomResponse>(messageTuple.Item2);
                case (int)ResponseCodes.STARTGAME:
                    return JsonSerializer.Deserialize<StartGameResponse>(messageTuple.Item2);
                case (int)ResponseCodes.GETROOMSTATE:
                    return JsonSerializer.Deserialize<GetRoomStateResponse>(messageTuple.Item2);
                case (int)ResponseCodes.LEAVEROOM:
                    return JsonSerializer.Deserialize<LeaveRoomResponse>(messageTuple.Item2);
                default:
                    // If the response is unknown
                    throw new TypeLoadException("IllegalResponseStruct");
            }
        }
    }

}
