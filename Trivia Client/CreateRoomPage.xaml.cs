﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for CreateRoomPage.xaml
    /// </summary>
    public partial class CreateRoomPage : Page
    {
        public CreateRoomPage()
        {
            InitializeComponent();
            // Init button functions
            this.Cancel.Click += cancelClick;
            this.CreateRoom.Click += createRoomClick;
            this.KeyDown += keyDown;
        }

        private void keyDown(object sender, KeyEventArgs e)
        {
            // Detect keyboard clicks for UX
            if (e.Key.ToString() == "Return")
            {
                createRoom();
            }
            else if (e.Key.ToString() == "Escape")
            {
                cancel();
            }
        }

        private void createRoomClick(object sender, RoutedEventArgs e)
        {
            createRoom();
        }

        private void cancelClick(object sender, RoutedEventArgs e)
        {
            cancel();
        }

        private void createRoom()
        {
            // Tells the server to create a room with the data given by the user
            CreateRoomRequest request = new CreateRoomRequest { roomName = this.NameInput.Text, maxUsers = uint.Parse(this.PlayersInput.Text), questionCount = uint.Parse(this.QuestionCountInput.Text), answerTimeout = uint.Parse(this.QuestionTimeInput.Text)};
            CreateRoomResponse response = (CreateRoomResponse)Communicator.Communicate(request, RequestCodes.CREATEROOM);
            if (response.status == 1)
            {
                // If the room was created it moves to the lobby page
                LobbyPage page = new LobbyPage(true, this.NameInput.Text);
                NavigationService.Navigate(page);
            }
        }

        private void cancel()
        {
            // Returns to the menu page
            MenuPage page = new MenuPage();
            NavigationService.Navigate(page);
        }
    }
}
