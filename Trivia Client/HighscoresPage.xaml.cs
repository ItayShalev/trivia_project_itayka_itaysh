﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for HighscoresPage.xaml
    /// </summary>
    public partial class HighscoresPage : Page
    {
        public HighscoresPage()
        {
            InitializeComponent();
            // Initiate button clicks
            this.Exit.Click += exitClick;
        }

        private void exitClick(object sender, RoutedEventArgs e)
        {
            // Returns to the main statistics page
            StatisticsPage page = new StatisticsPage();
            NavigationService.Navigate(page);
        }
    }
}
