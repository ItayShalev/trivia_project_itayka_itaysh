﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for JoinRoomPage.xaml
    /// </summary>
    public partial class JoinRoomPage : Page
    {
        private bool _closeThread;
        public JoinRoomPage()
        {
            InitializeComponent();
            // Initialize button clicks
            this.Cancel.Click += cancelClick;
            this.Join.Click += joinClick;

            this.Error.Visibility = Visibility.Hidden; // Hide error message

            updateRooms(); // Show the available rooms

            // Start the room updating thread
            this._closeThread = false;
            Thread roomUpdater = new Thread(new ThreadStart(updateRoomsThread));
            roomUpdater.Start();
        }

        private void joinClick(object sender, RoutedEventArgs e)
        {
            if (this.Rooms.SelectedIndex == -1)
            {
                this.Error.Visibility = Visibility.Hidden; // Hide the error if no room is selected
            }
            else
            {
                // Tell the server to join a room by the room name
                JoinRoomRequest request = new JoinRoomRequest { roomid = Utils.getRoomIdByName((string)((ListBoxItem)this.Rooms.SelectedItem).Content) };
                JoinRoomResponse response = (JoinRoomResponse)Communicator.Communicate(request, RequestCodes.JOINROOM);

                if (response.status == 1)
                {
                    // If it joined the room the room updater stops and it goes to the lobby page
                    this._closeThread = true;
                    LobbyPage page = new LobbyPage(false, (string)((ListBoxItem)this.Rooms.SelectedItem).Content);
                    NavigationService.Navigate(page);
                }
                else
                {
                    // Show the error if it couldn't join the room
                    this.Error.Visibility = Visibility.Visible;
                }
            }
        }

        private void cancelClick(object sender, RoutedEventArgs e)
        {
            // Stops the updater and goes to the menu page
            this._closeThread = true;
            MenuPage page = new MenuPage();
            NavigationService.Navigate(page);
        }

        private void updateRooms()
        {
            // Requests the list of rooms
            Request request = new Request();
            GetRoomsResponse response = (GetRoomsResponse)Communicator.Communicate(request, RequestCodes.GETROOMS);

            this.Rooms.Items.Clear(); // Clears the existing rooms

            if (response.rooms == null)
            {
                // If there are no open rooms it quits
                return;
            }

            foreach (RoomData room in response.rooms)
            {
                // Adds each room in the list to the item box
                ListBoxItem currRoom = new ListBoxItem();
                currRoom.Content = room.name;
                this.Rooms.Items.Add(currRoom);
            }
        }

        private void updateRoomsThread()
        {
            while (!this._closeThread)
            {
                // While the page is active, it updates the rooms every 3 seconds
                Thread.Sleep(3000);
                if ((!this._closeThread))
                {
                    // Uses invoke to get ownership over the itembox
                    this.Dispatcher.Invoke(() =>
                    {
                        updateRooms();
                    });
                }
            }
        }
    }
}
