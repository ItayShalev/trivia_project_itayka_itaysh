﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for LobbyPage.xaml
    /// </summary>
    public partial class LobbyPage : Page
    {
        private bool _closeThread;
        public LobbyPage(bool isAdmin, string roomName)
        {
            InitializeComponent();
            // Initiate button clicks
            this.LeavePlayer.Click += leavePlayerClick;
            this.CloseRoomAdmin.Click += leaveAdminClick;

            this.RoomName.Content = roomName; // Set the room name

            if (isAdmin)
            {
                // Show admin controls
                this.LeavePlayer.Visibility = Visibility.Hidden;
            }
            else
            {
                // Show member controls
                this.CloseRoomAdmin.Visibility = Visibility.Hidden;
                this.StartAdmin.Visibility = Visibility.Hidden;
            }

            initPlayers(); // Show the players in the room

            // Start the players updater
            this._closeThread = false;
            Thread updatePlayersThread = new Thread(new ThreadStart(updatePlayers));
            updatePlayersThread.Start();
        }

        private void leaveAdminClick(object sender, RoutedEventArgs e)
        {
            // Tell the server to close the room
            Request request = new Request{ };
            CloseRoomResponse response = (CloseRoomResponse)Communicator.Communicate(request, RequestCodes.CLOSEROOM);

            if (response.status == 1)
            {
                // If the room closed it stops the updater and goes to the menu page
                this._closeThread = true;
                MenuPage page = new MenuPage();
                NavigationService.Navigate(page);
            }
        }

        private void leavePlayerClick(object sender, RoutedEventArgs e)
        {
            // Tell the server to leave the room
            Request request = new Request { };
            LeaveRoomResponse response = (LeaveRoomResponse)Communicator.Communicate(request, RequestCodes.LEAVEROOM);

            if (response.status == 1)
            {
                // If you leave the room it stops the updater and goes to the join room page
                this._closeThread = true;
                JoinRoomPage page = new JoinRoomPage();
                NavigationService.Navigate(page);
            }
        }

        private void initPlayers()
        {
            // Get the list of players in the room
            Request request = new Request { };
            object response = Communicator.Communicate(request, RequestCodes.GETROOMSTATE);

            // Check if the room was closed
            if (response.GetType() == typeof(CloseRoomResponse))
            {
                // Stops the updater and goes to the join room page
                this._closeThread = true;
                JoinRoomPage page = new JoinRoomPage();
                NavigationService.Navigate(page);
                return;
            }

            // Changes the admin to the first player in the list
            StackPanel admin = (StackPanel)this.Players.Items.GetItemAt(0);
            ((TextBlock)admin.Children[1]).Text = ((GetRoomStateResponse)response).players[0];

            this.Players.Items.Clear(); // Clears all players

            this.Players.Items.Add(admin); // Adds teh admin

            foreach (string player in ((GetRoomStateResponse)response).players.Skip<string>(1))
            {
                // Adds each player in the list to the item box
                Label currPlayer = new Label();
                currPlayer.Content = player;
                currPlayer.Margin = new Thickness(33, 0, 0, 0);
                this.Players.Items.Add(currPlayer);
            }
        }

        private void updatePlayers()
        {
            // While the page is active
            while (!this._closeThread)
            {
                // Updates the room state every 3 seconds
                Thread.Sleep(3000);
                if ((!this._closeThread))
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        initPlayers();
                    });
                }
            }
        }
    }
}
