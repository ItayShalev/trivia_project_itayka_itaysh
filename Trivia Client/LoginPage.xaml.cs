﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            InitializeComponent();
            // Init button clicks
            this.Login.Click += loginClick;
            this.Cancel.Click += cancelClick;
            this.KeyDown += keyDown;
            this.Error.Visibility = Visibility.Hidden; // Hide the error message
        }

        private void keyDown(object sender, KeyEventArgs e)
        {
            // Detects keystrokes for UX
            if (e.Key.ToString() == "Return")
            {
                login();
            }
            else if (e.Key.ToString() == "Escape")
            {
                cancel();
            }
        }

        private void cancelClick(object sender, RoutedEventArgs e)
        {
            cancel();
        }

        private void loginClick(object sender, RoutedEventArgs e)
        {
            login();
        }

        private void login()
        {
            // Requests login from the server
            LoginRequest request = new LoginRequest { username = this.UsernameInput.Text, password = this.PasswordInput.Password };
            LoginResponse response = (LoginResponse)Communicator.Communicate(request, RequestCodes.LOGIN);
            if (response.status == 1)
            {
                // If the login was successful it goes to the menu page
                MenuPage page = new MenuPage();
                NavigationService.Navigate(page);
            }
            else
            {
                // Show and error if it couldn't log in
                this.Error.Visibility = Visibility.Visible;
            }
        }

        private void cancel()
        {
            // Returns to the welcome page
            WelcomePage page = new WelcomePage();
            NavigationService.Navigate(page);
        }
    }
}
