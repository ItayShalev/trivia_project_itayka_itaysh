﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for MenuPage.xaml
    /// </summary>
    public partial class MenuPage : Page
    {
        public MenuPage()
        {
            InitializeComponent();
            // Init button clicks
            this.Logout.Click += logoutClick;
            this.Statistics.Click += statisticsClick;
            this.CreateRoom.Click += createRoomClick;
            this.JoinRoom.Click += joinRoomClick;
        }

        private void joinRoomClick(object sender, RoutedEventArgs e)
        {
            // Goes to the join room page
            JoinRoomPage page = new JoinRoomPage();
            NavigationService.Navigate(page);
        }

        private void createRoomClick(object sender, RoutedEventArgs e)
        {
            // Goes to the create room page
            CreateRoomPage page = new CreateRoomPage();
            NavigationService.Navigate(page);
        }

        private void statisticsClick(object sender, RoutedEventArgs e)
        {
            // Goes to the statistics menu page
            StatisticsPage page = new StatisticsPage();
            NavigationService.Navigate(page);
        }

        private void logoutClick(object sender, RoutedEventArgs e)
        {
            // Requests log out from the server
            Request request = new Request();
            LogoutResponse response = (LogoutResponse)Communicator.Communicate(request, RequestCodes.LOGOUT);
            if (response.status == 1)
            {
                // If it logged out successfully it returns to the welcome page
                WelcomePage page = new WelcomePage();
                NavigationService.Navigate(page);
            }
        }
    }
}
