﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_Client
{
    enum RequestCodes
    {
        LOGIN = 1,
        SIGNUP,
        LOGOUT,
        GETROOMS,
        GETPLAYERSINROOM,
        JOINROOM,
        CREATEROOM,
        GETSTATS,
        CLOSEROOM,
        STARTGAME,
        GETROOMSTATE,
        LEAVEROOM
    }

    struct Request
    {

    }

    struct LoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    struct SignupRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
    }

    struct GetPlayersInRoomRequest
    {
        public uint roomid { get; set; }
    }

    struct JoinRoomRequest
    {
        public uint roomid { get; set; }
    }

    struct CreateRoomRequest
    {
        public string roomName { get; set; }
        public uint maxUsers { get; set; }
        public uint questionCount { get; set; }
        public uint answerTimeout { get; set; }
    }
}
