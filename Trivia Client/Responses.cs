﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_Client
{
    enum ResponseCodes
    {
        ERROR = 0,
        LOGIN,
        SIGNUP,
        LOGOUT,
        GETROOMS,
        GETPLAYERSINROOM,
        JOINROOM,
        CREATEROOM,
        GETSTATS,
        CLOSEROOM,
        STARTGAME,
        GETROOMSTATE,
        LEAVEROOM
    }

    struct ErrorResponse
    {
        public string message { get; set; }
    }

    struct LoginResponse
    {
        public uint status { get; set; }
    }

    struct SignupResponse
    {
        public uint status { get; set; }
    }

    struct LogoutResponse
    {
        public uint status { get; set; }
    }

    struct GetRoomsResponse
    {
        public uint status { get; set; }
        public List<RoomData> rooms { get; set; }
    }

    struct GetPlayersInRoomResponse
    {
        public List<string> players { get; set; }
    }

    struct JoinRoomResponse
    {
        public uint status { get; set; }
    }

    struct CreateRoomResponse
    {
        public uint status { get; set; }
    }

    struct GetStatisticsResponse
    {
        public uint status { get; set; }
        public List<string> statistics { get; set; }
    }

    struct CloseRoomResponse
    {
        public uint status { get; set; }
    }

    struct StartGameResponse
    {
        public uint status { get; set; }
    }

    struct GetRoomStateResponse
    {
        public uint status { get; set; }
        public List<string> players { get; set; }
        public uint questionCount { get; set; }
        public float answerTimeout { get; set; }
    }

    struct LeaveRoomResponse
    {
        public uint status { get; set; }
    }
}
