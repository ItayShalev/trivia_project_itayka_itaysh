﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for SignupPage.xaml
    /// </summary>
    public partial class SignupPage : Page
    {
        public SignupPage()
        {
            InitializeComponent();
            // Init button clicks
            this.Cancel.Click += cancelClick;
            this.Signup.Click += signupClick;
            this.KeyDown += keyDown;
            this.Error.Visibility = Visibility.Hidden; // Hide the error message
        }

        private void keyDown(object sender, KeyEventArgs e)
        {
            // Detect keystrokes for UX
            if (e.Key.ToString() == "Return")
            {
                signup();
            }
            else if (e.Key.ToString() == "Escape")
            {
                cancel();
            }
        }

        private void signupClick(object sender, RoutedEventArgs e)
        {
            signup();
        }

        private void cancelClick(object sender, RoutedEventArgs e)
        {
            cancel();
        }

        private void signup()
        {
            // Request sign up from the server
            SignupRequest request = new SignupRequest { username = this.UsernameInput.Text, password = this.PasswordInput.Password, email = this.EmailInput.Text };
            SignupResponse response = (SignupResponse)Communicator.Communicate(request, RequestCodes.SIGNUP);
            if (response.status == 1)
            {
                // If the signing up was successful it logins and goes to the menu page
                MenuPage page = new MenuPage();
                NavigationService.Navigate(new MenuPage());
            }
            else
            {
                // If the login was unsuccessful it shows an error
                this.Error.Visibility = Visibility.Visible;
            }
        }

        private void cancel()
        {
            // Returns to the welcome page
            WelcomePage page = new WelcomePage();
            NavigationService.Navigate(page);
        }
    }
}
