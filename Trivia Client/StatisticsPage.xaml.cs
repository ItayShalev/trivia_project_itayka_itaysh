﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for StatisticsPage.xaml
    /// </summary>
    public partial class StatisticsPage : Page
    {
        public StatisticsPage()
        {
            InitializeComponent();
            // Init button clicks
            this.Exit.Click += exitClick;
            this.MyStatistics.Click += myStatisticsClick;
            this.Highscores.Click += highscoresClick;
        }

        private void highscoresClick(object sender, RoutedEventArgs e)
        {
            // Goes to the highscores page
            HighscoresPage page = new HighscoresPage();
            NavigationService.Navigate(page);
        }

        private void myStatisticsClick(object sender, RoutedEventArgs e)
        {
            // Goes to the user statistics page
            MyStatisticsPage page = new MyStatisticsPage();
            NavigationService.Navigate(page);
        }

        private void exitClick(object sender, RoutedEventArgs e)
        {
            // Returns to the menu page
            MenuPage page = new MenuPage();
            NavigationService.Navigate(page);
        }
    }
}
