﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia_Client
{
    struct RoomData
    {
        public uint id { get; set; }
        public string name { get; set; }
        public uint maxPlayers { get; set; }
        public uint questionTime { get; set; }
        public int isActive { get; set; }
    }

    class Utils
    {
        public static uint getRoomIdByName(string roomName)
        {
            // Requests all available rooms
            Request request = new Request();
            GetRoomsResponse response = (GetRoomsResponse)Communicator.Communicate(request, RequestCodes.GETROOMS);

            foreach (RoomData room in response.rooms)
            {
                if (room.name == roomName)
                {
                    // Checks if the name matches and returns the room id
                    return room.id;
                }
            }

            return 0;
        }
    }
}
