﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for WelcomePage.xaml
    /// </summary>
    public partial class WelcomePage : Page
    {
        public WelcomePage()
        {
            InitializeComponent();
            // Init button clicks
            this.Login.Click += loginClick;
            this.Exit.Click += exitClick;
            this.Signup.Click += signupClick;
        }

        private void signupClick(object sender, RoutedEventArgs e)
        {
            // Goes to the sign up page
            SignupPage page = new SignupPage();
            NavigationService.Navigate(page);
        }

        private void exitClick(object sender, RoutedEventArgs e)
        {
            // Shuts the program down
            System.Windows.Application.Current.Shutdown();
        }

        private void loginClick(object sender, RoutedEventArgs e)
        {
            // Goes to the login page
            LoginPage page = new LoginPage();
            NavigationService.Navigate(page);
        }
    }
}
