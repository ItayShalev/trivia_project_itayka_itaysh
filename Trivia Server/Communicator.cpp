#include "Communicator.h"

Communicator::Communicator(RequestHandlerFactory& factory)
	: _handlerFactory(factory), _serverSocket(NULL)
{

}

Communicator::~Communicator()
{
	// Goes over each client in the clients
	for (auto client : this->_clients)
	{
		try
		{
			// Closes the socket and frees the handler
			closesocket(client.first);
			delete client.second;
		}
		catch (...) // If one of them failed
		{
			throw std::exception("Error: Could not remove a client.");
		}
	}

	try
	{
		closesocket(this->_serverSocket); // Closes the server socket
	}
	catch (...)
	{
		throw std::exception("Error: Could not close server sockets.");
	}
}

void Communicator::startHandleRequests()
{
	std::thread listener(&Communicator::bindAndListen, this);
	listener.detach();
}

void Communicator::bindAndListen()
{
	struct sockaddr_in socketAddress = { 0 }; // Creating the socket attributes
	this->_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); // Creating the socket for the server

	if (this->_serverSocket == INVALID_SOCKET)
	{
		throw std::exception("Error: Could not open the server side socket.");
	}

	socketAddress.sin_port = htons(PORT); // port that server will listen for
	socketAddress.sin_family = AF_INET; // must be AF_INET
	socketAddress.sin_addr.s_addr = INADDR_ANY; // When there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->_serverSocket, (struct sockaddr*)& socketAddress, sizeof(socketAddress)) == SOCKET_ERROR)
	{
		throw std::exception("Error: Could not bind socket to the given ip and port.");
	}

	// Start listening for incoming requests of clients
	if (listen(this->_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception("Error: Could not start listening.");
	}

	std::cout << "Listening on port " << PORT << "..." << std::endl;

	while (true)
	{
		// Waiting for a new client to connect
		Socket clientSocket = accept(this->_serverSocket, NULL, NULL);
		std::cout << "New client connected" << std::endl;
		if (clientSocket == INVALID_SOCKET)
		{
			throw std::exception("Error: Could not accept new client.");
		}

		// Creates an handler for the client and inserts the client into the map of clients
		LoginRequestHandler* clientHandler = _handlerFactory.createLoginRequestHandler();
		this->_clientsMutex.lock();
		this->_clients.insert(std::pair<Socket, IRequestHandler*>(clientSocket, (IRequestHandler*)clientHandler));
		this->_clientsMutex.unlock();

		// Creates a thread for the client which handles the login/signup
		std::thread client(&Communicator::handleNewClient, this);
		client.detach();
	}
}

void Communicator::handleNewClient()
{
	RequestInfo request;
	RequestResult requestRes;
	std::vector<unsigned char> responseBuffer;

	this->_clientsMutex.lock();
	std::pair<Socket, IRequestHandler*> client = *(this->_clients.rbegin()); // Gets the last client who connected
	this->_clientsMutex.unlock();

	while (true)
	{
		try
		{
			request = getMessage(client.first); // Gets a new messgae from the client
		}
		catch (...)
		{
			break;
		}
		if (client.second->isRequestRelevent(request)) // If the request is relevant
		{
			requestRes = client.second->handleRequest(request); // Handles the request
			responseBuffer = requestRes.response; // Saves the response buffer to send it to the client
			client.second = requestRes.newHandler; // Updates the clients map with a new handler
		}
		else
		{
			// Creates an error buffer and sends it to the client
			responseBuffer = JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "Wrong request for handler type." });
		}
		send(client.first, (char*)&responseBuffer[0], responseBuffer.size(), 0); // Sends the response to the user
		std::cout << "Message sent" << std::endl;
	}
}

RequestInfo Communicator::getMessage(const Socket& sock)
{
	char codeBuffer;
	char sizeBuffer[SIZE_LENGTH] = { 0 };
	char* data = nullptr;
	int code;
	int size;
	int isConnected;

	// Get the message code
	isConnected = recv(sock, &codeBuffer, CODE_LENGTH, 0);
	code = (int)codeBuffer;

	// Get the data size
	isConnected = recv(sock, sizeBuffer, SIZE_LENGTH, 0);
	if (sizeBuffer[0] == 0 && sizeBuffer[1] == 0 && sizeBuffer[2] == 0 && sizeBuffer[3] == 0)
	{
		size = 0;
	}
	else
	{
		size = int((sizeBuffer[0]) << 24 | (sizeBuffer[1]) << 16 | (sizeBuffer[2]) << 8 | (sizeBuffer[3]));
	}
	
	// Get the data
	if (size != 0)
	{
		data = new char[size];
		isConnected = recv(sock, data, size, 0);
	}

	if (isConnected == -1)
	{
		throw std::exception();
	}

	// Returns a struct which composes the data
	return RequestInfo{ code, std::time(0), std::vector<unsigned char>(data, data + size) };
}
