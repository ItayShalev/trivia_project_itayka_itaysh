#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <thread>
#include <mutex>
#include <iostream>

#include "WSAInitializer.h"
#include "Requests.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"

#define CODE_LENGTH 1
#define SIZE_LENGTH 4
#define PORT 2212

typedef SOCKET Socket;

class Communicator
{
public:
	Communicator(RequestHandlerFactory& factory);
	~Communicator();

	void startHandleRequests();

private:
	std::map<Socket, IRequestHandler*> _clients;
	Socket _serverSocket;
	std::mutex _clientsMutex;
	RequestHandlerFactory& _handlerFactory;

	/* Binds a socket to the PORT and listens for new clients that connecto to the socket.
	Calls the handleNewClient functio for every new client.
	*/
	void bindAndListen();

	/* Handles ebery new client - meaning checks if the current handler is currect, calls the handleRequest 
	function of the handler and sends the response back to the client
	*/
	void handleNewClient();
	
	/* Gets a mesage from the socket and parses its data into parts according to the message diagram.
	Returns the RequestInfo struct that has the request information sorted to fields.
	*/
	RequestInfo getMessage(const Socket& sock);
};