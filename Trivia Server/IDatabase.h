#pragma once

#include <string>
#include <list>

#include "Statistics.h"
#include "Question.h"
#include <vector>

class IDatabase
{
public:
	virtual bool doesUserExists(std::string username) = 0;
	virtual bool doesPasswordMatch(std::string password, std::string username) = 0;
	virtual void addNewUser(std::string email, std::string username, std::string password) = 0;

	virtual std::list<Question> getQuestions(int count) = 0;

	virtual float getAverageAnswerTime(std::string username) = 0;
	virtual int getNumOfCorrectAnswers(std::string username) = 0;
	virtual int getNumOfTotalAnswers(std::string username) = 0;
	virtual int getNumOfPlayerGames(std::string username) = 0;
	virtual Statistics getAllStatistics(std::string username) = 0;
	virtual std::vector<Statistics> getHighestPlayersStatistics() = 0;
};