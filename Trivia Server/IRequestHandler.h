#pragma once
#include "JsonRequestPacketDeserializer.h"


typedef struct RequestInfo
{
	int id;
	time_t receivalTime;
	std::vector<unsigned char> buffer;
} RequestInfo;


struct RequestResult;
struct RequestInfo;
class IRequestHandler
{
public:

	/* Checks if the request is relevent for the current handler */
	virtual bool isRequestRelevent(RequestInfo) = 0;
	/* Handles the current request accurding to the current handler */
	virtual RequestResult handleRequest(RequestInfo) = 0;
};

/* The response struct that is returned after finishing the hrequest handling */
typedef struct RequestResult
{
	std::vector<unsigned char> response;
	IRequestHandler* newHandler;

} RequestResult;