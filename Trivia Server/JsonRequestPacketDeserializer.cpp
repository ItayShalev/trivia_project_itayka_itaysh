#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer)
{
	LoginRequest generatedRequest;
	auto jsonData = json::from_msgpack(buffer);
	generatedRequest.username = jsonData["username"];
	generatedRequest.password = jsonData["password"];
	
	return generatedRequest;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer)
{
	SignupRequest generatedRequest;
	auto jsonData = json::from_msgpack(buffer);
	generatedRequest.username = jsonData["username"];
	generatedRequest.password = jsonData["password"];
	generatedRequest.email = jsonData["email"];

	std::cout << "Username: " << generatedRequest.username << ", Password:" << generatedRequest.password << std::endl;

	return generatedRequest;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer)
{
	GetPlayersInRoomRequest generatedRequest;
	auto jsonData = json::from_msgpack(buffer);
	generatedRequest.roomid = jsonData["roomid"];
	return generatedRequest;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
	JoinRoomRequest generatedRequest;
	auto jsonData = json::from_msgpack(buffer);
	generatedRequest.roomid = jsonData["roomid"];
	return generatedRequest;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
	CreateRoomRequest generatedRequest;
	auto jsonData = json::from_msgpack(buffer);
	generatedRequest.roomName = jsonData["roomName"];
	generatedRequest.maxUsers = jsonData["maxUsers"];
	generatedRequest.questionCount = jsonData["questionCount"];
	generatedRequest.answerTimeout = jsonData["answerTimeout"];

	return generatedRequest;
}
