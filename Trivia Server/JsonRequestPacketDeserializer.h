#pragma once
#include "Requests.h"
#include "json.hpp"

#include <iostream>

#define MSG_HEADER_LEN 5 // Length of the header - the message length (1 byte) and the message size (4 bytes)

typedef nlohmann::json json;

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(std::vector<unsigned char> buffer);
	static SignupRequest deserializeSignupRequest(std::vector<unsigned char> buffer);

	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer); 
};