#include "JsonResponsePacketSerializer.h"


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
	json jsonResponse = { {"message", response.message} };
	return formatMessage(jsonResponse, ResponseCodes::error);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
	json jsonResponse = { {"status", response.status} };
	return formatMessage(jsonResponse, ResponseCodes::login);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
	json jsonResponse = { {"status", response.status} };
	return formatMessage(jsonResponse, ResponseCodes::signup);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
	json jsonResponse = {{"status", response.status}};
	return formatMessage(jsonResponse, ResponseCodes::logout);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
	json jsonResponse = { {"status", response.status} };
	for (auto room : response.rooms)
	{
		jsonResponse["rooms"].push_back(room);
	}
	return formatMessage(jsonResponse, ResponseCodes::getRooms);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
	json jsonResponse = { {"status", response.status} };
	for (auto player : response.players)
	{
		jsonResponse["players"].push_back(player);
	}
	return formatMessage(jsonResponse, ResponseCodes::getPlayersInRoom);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
	json jsonResponse = { {"status", response.status} };
	return formatMessage(jsonResponse, ResponseCodes::joinRoom);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
	json jsonResponse = { {"status", response.status} };
	return formatMessage(jsonResponse, ResponseCodes::createRoom);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse response)
{
	json jsonResponse = { {"status", response.status}, {"statitics", response.statistics} };
	return formatMessage(jsonResponse, ResponseCodes::getStats);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response)
{
	json jsonResponse = { {"status", response.status} };
	return formatMessage(jsonResponse, ResponseCodes::closeRoom);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse response)
{
	json jsonResponse = { {"status", response.status} };
	return formatMessage(jsonResponse, ResponseCodes::startGame);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	json jsonResponse = { {"status", response.status}};
	for (auto player : response.players)
	{
		jsonResponse["players"].push_back(player);
	}
	jsonResponse["questionCount"] = response.questionCount;
	jsonResponse["answerTimeout"] = response.answerTimeout;
	return formatMessage(jsonResponse, ResponseCodes::getRoomState);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response)
{
	json jsonResponse = { {"status", response.status} };
	return formatMessage(jsonResponse, ResponseCodes::leaveRoom);
}


std::vector<unsigned char> JsonResponsePacketSerializer::intToBytes(int paramInt)
{
	int i = 0;
	std::vector<unsigned char> arrayOfBytes(4); // Initilizes a 4 byte array
	for (i = 0; i < 4; i++) // Goes over all bytes
	{
		arrayOfBytes[3 - i] = (paramInt >> (i * 8)); // Inserts the shifted int to the buffer
	}
	return arrayOfBytes;

	// Source code from: https://stackoverflow.com/questions/5585532/c-int-to-byte-array
}


std::vector<unsigned char> JsonResponsePacketSerializer::formatMessage(const json& response, const ResponseCodes& code)
{
	// Buffer Creation
	std::vector<unsigned char> serializedResponse; // Create the output buffer
	std::vector<unsigned char> serializedJsonResponse = json::to_msgpack(response); // Convert the json object to a buffer
	std::vector<unsigned char> messageLength = intToBytes(serializedJsonResponse.size()); // Converts the json buffer length to a buffer

	// Buffer Concatenation
	serializedResponse.push_back((unsigned char)code); // Inserts the message code to the buffer
	serializedResponse.insert(serializedResponse.end(), messageLength.begin(), messageLength.end()); // Concatenates the message length buffer to the output buffer
	serializedResponse.insert(serializedResponse.end(), serializedJsonResponse.begin(), serializedJsonResponse.end()); // Concatenates the message buffer to the output buffer

	return serializedResponse;
}

