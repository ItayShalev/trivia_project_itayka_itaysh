#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string username) : _username(username)
{
	// Remains empty because of the initialization list.
}

std::string LoggedUser::getUsername() const
{
	// Returns the username stored in the class
	return this->_username;
}

bool LoggedUser::operator==(const LoggedUser& other)
{
	// == operator is nessecery in order to remove this class from a vector
	return this->getUsername() == other.getUsername();
}
