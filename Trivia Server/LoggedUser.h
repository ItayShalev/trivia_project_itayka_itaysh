#pragma once

#include <string>

class LoggedUser
{
public:
	LoggedUser(std::string username);

	std::string getUsername() const;

	bool operator==(const LoggedUser& other); // Must have in order to remove from a vector

private:
	std::string _username;
};