#include "LoginManager.h"

LoginManager::LoginManager(IDatabase* database) : _database(database)
{
	// Remains empty because of the initialization list.
}

bool LoginManager::signup(std::string username, std::string email, std::string password)
{
	// Checks if the user is already signed up
	if (this->_database->doesUserExists(username))
	{
		return false;
	}

	this->_database->addNewUser(email, username, password); // Inserts him into the database
	this->_loggedUsers.push_back(LoggedUser(username)); // Adds him to the logged users list

	return true;
}

bool LoginManager::login(std::string username, std::string password)
{
	// Checks if the password is correct
	if (!this->_database->doesPasswordMatch(password, username) || std::find(this->_loggedUsers.begin(), this->_loggedUsers.end(), LoggedUser(username)) != this->_loggedUsers.end())
	{
		return false;
	}

	this->_loggedUsers.push_back(LoggedUser(username)); // Logs the user to the list

	return true;
}

bool LoginManager::logout(std::string username)
{
	auto userIterator = std::find(this->_loggedUsers.begin(), this->_loggedUsers.end(), LoggedUser(username)); // Gets the user location in the vector
	
	if (userIterator == this->_loggedUsers.end()) // Checks if that user is logged in
	{
		return false;
	}

	this->_loggedUsers.erase(userIterator); // Removes the user from the vector

	return true;
}

void LoginManager::refresh(std::string username)
{
	auto userIterator = std::find(this->_loggedUsers.begin(), this->_loggedUsers.end(), LoggedUser(username)); // Gets the user location in the vector

	this->_loggedUsers.erase(userIterator); // Removes the user from the vector
	this->_loggedUsers.push_back(LoggedUser(username));
}

std::vector<LoggedUser> LoginManager::getLoggedUsers()
{
	return this->_loggedUsers;
}
