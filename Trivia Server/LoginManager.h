#pragma once

#include <string>
#include <vector>

#include "IDatabase.h"
#include "LoggedUser.h"


/* Manages the data of the database and the logged users, removing and adding data to it accordingly.*/
class LoginManager
{
public:
	LoginManager(IDatabase* database);

	bool signup(std::string username, std::string email, std::string password);
	bool login(std::string username, std::string password);
	bool logout(std::string username);
	/* This function is called when the user already exists but a new menu manager needs to be created using the handler factory.
	 Basically moved the user to the front of the _loggedUsers vector right before creating a new menu handler, needed to be because of the process of creating the handler. */
	void refresh(std::string username);
	std::vector<LoggedUser> getLoggedUsers();

private:
	IDatabase* _database;
	std::vector<LoggedUser> _loggedUsers;
};