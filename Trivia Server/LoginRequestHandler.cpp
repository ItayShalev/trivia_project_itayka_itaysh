#include "LoginRequestHandler.h"
#include <iostream>


LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& factory, LoginManager& loginManager) : _handlerFactory(factory), _loginManager(loginManager)
{
}

bool LoginRequestHandler::isRequestRelevent(RequestInfo requestInfo)
{
	return requestInfo.id == (int)RequestCodes::Login || requestInfo.id == (int)RequestCodes::Signup; // A 'relevent' request is a login or signup request.
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestRes;
	try
	{
		requestRes = (requestInfo.id == (int)RequestCodes::Login) ? login(requestInfo) : signup(requestInfo); // I already know that the message is login/signup, so if it's not a login message it has to be signup.
	}
	catch (std::exception exception)
	{
		ErrorResponse res;
		std::cerr << "login handler error" << exception.what() << std::endl;
		res.message = std::string("login handler error") + exception.what();
		requestRes.newHandler = nullptr;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	}
	return requestRes;
}

RequestResult LoginRequestHandler::login(RequestInfo requestInfo)
{
	RequestResult requestRes;
	LoginResponse res;
	auto deserializedRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(requestInfo.buffer);
	if (_loginManager.login(deserializedRequest.username, deserializedRequest.password))
	{
		requestRes.newHandler = (IRequestHandler*)_handlerFactory.createMenuRequestHandler();
		res.status = SUCCESS;
	}
	else
	{
		requestRes.newHandler = this;
		res.status = FALIURE;
	}
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return requestRes;
}

RequestResult LoginRequestHandler::signup(RequestInfo requestInfo)
{
	RequestResult requestRes;
	json response;
	SignupResponse res;
	auto deserializedRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(requestInfo.buffer);
	if (_loginManager.signup(deserializedRequest.username, deserializedRequest.password, deserializedRequest.email))
	{
		requestRes.newHandler = (IRequestHandler*)_handlerFactory.createMenuRequestHandler();
		res.status = SUCCESS;
	}
	else
	{
		requestRes.newHandler = (IRequestHandler*)_handlerFactory.createLoginRequestHandler();
		res.status = FALIURE;
	}
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return requestRes;
}
