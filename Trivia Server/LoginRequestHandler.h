#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"



class RequestHandlerFactory;

class LoginRequestHandler : IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory& factory, LoginManager& loginManager);

	bool isRequestRelevent(RequestInfo requestInfo);
	RequestResult handleRequest(RequestInfo requestInfo);
private:
	RequestResult login(RequestInfo requestInfo);
	RequestResult signup(RequestInfo requestInfo);

	LoginManager& _loginManager;
	RequestHandlerFactory& _handlerFactory;
};