#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory& requestHandlerFactorty, RoomManager& roomManager, StatisticsManager& statisticasManager, LoggedUser user)
	: _roomManager(roomManager), _statisticsManager(statisticasManager), _handlerFactory(requestHandlerFactorty), _user(user)
{
}

bool MenuRequestHandler::isRequestRelevent(RequestInfo requestInfo)
{
	std::set<RequestCodes> validCodes = { RequestCodes::Logout, RequestCodes::CreateRoom, RequestCodes::GetRooms,
		RequestCodes::GetPlayersInRoom, RequestCodes::JoinRoom, RequestCodes::GetStatistics }; // A set of all the valid codes that can be used in the menu.
	return validCodes.find((RequestCodes)requestInfo.id) != validCodes.end(); // Uses the 'find' function, returns the past-the-end iterator if element not found.
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestRes;
	try
	{
		switch (requestInfo.id)
		{
		case (int)RequestCodes::Logout:
			requestRes = logout(requestInfo);
			break;
		case (int)RequestCodes::CreateRoom:
			requestRes = createRoom(requestInfo);
			break;
		case (int)RequestCodes::GetRooms:
			requestRes = getRooms(requestInfo);
			break;
		case (int)RequestCodes::GetPlayersInRoom:
			requestRes = getPlayersInRoom(requestInfo);
			break;
		case (int)RequestCodes::JoinRoom:
			requestRes = joinRoom(requestInfo);
			break;
		case (int)RequestCodes::GetStatistics:
			requestRes = getStatistics(requestInfo);
			break;
		}
	}
	catch (std::exception& exception)
	{
		ErrorResponse res;
		std::cerr << "menu handler error: " << exception.what() << std::endl;
		res.message = std::string("menu handler error: ") + exception.what();
		requestRes.newHandler = nullptr;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	}
	return requestRes;
}

RequestResult MenuRequestHandler::logout(RequestInfo requestInfo)
{
	RequestResult requestRes;
	LogoutResponse res;
	if (_handlerFactory.getLoginManager().logout(_user.getUsername()))
	{
		requestRes.newHandler = (IRequestHandler*)_handlerFactory.createLoginRequestHandler();
		res.status = SUCCESS;
	}
	else
	{
		requestRes.newHandler = this;
		res.status = FALIURE;
	}
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return requestRes;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo requestInfo)
{
	RequestResult requestRes;
	GetRoomsResponse res;
	res.rooms = _roomManager.getRooms();
	res.status = SUCCESS;
	requestRes.newHandler = this;
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return requestRes;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo requestInfo)
{
	RequestResult requestRes;
	GetPlayersInRoomResponse res;
	auto deserializedRequest = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(requestInfo.buffer);
	std::vector<std::string> roomPlayers = _roomManager.getRoom(deserializedRequest.roomid).getAllUsers();
	if (std::find(roomPlayers.begin(), roomPlayers.end(), "Error") != roomPlayers.end())
	{
		res.status = SUCCESS;
		res.players = roomPlayers;
	}
	else
	{
		res.status = FALIURE;
		res.players = std::vector<std::string>();
	}
	requestRes.newHandler = this;
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return requestRes;
}

RequestResult MenuRequestHandler::getStatistics(RequestInfo requestInfo)
{
	RequestResult requestRes;
	GetStatisticsResponse res;
	res.statistics = _statisticsManager.getStatistics(_user.getUsername());
	if (!res.statistics.empty())
	{
		res.status = SUCCESS;
	}
	else
	{
		res.status = FALIURE;
	}
	requestRes.newHandler = this;
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return requestRes;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo requestInfo)
{
	RequestResult requestRes;
	JoinRoomResponse res;
	auto deserializedRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(requestInfo.buffer);
	Room& room = _roomManager.getRoom(deserializedRequest.roomid);
	std::vector<std::string> users = room.getAllUsers();
	if (std::find(users.begin(), users.end(), "Error") == users.end() && room.getMetadeta().maxPlayers > room.getAllUsers().size())
	{
		room.addUser(_user);
		res.status = SUCCESS;

		auto newHandler = (IRoomRequestHandler*)_handlerFactory.createRoomMemberRequestHandler();
		newHandler->setCredentials(room, _user); // Function has to be called to set the data of the handler. can't be set from handler factory because function gets no parameters.
		requestRes.newHandler = (IRequestHandler*)newHandler;
	}
	else
	{
		res.status = FALIURE;
		requestRes.newHandler = this;
	}
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return requestRes;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo requestInfo)
{
	RequestResult requestRes;
	CreateRoomResponse res;
	auto deserializedRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(requestInfo.buffer);
	Room room = _roomManager.createRoom(_user.getUsername(), deserializedRequest.maxUsers, deserializedRequest.answerTimeout, deserializedRequest.questionCount, deserializedRequest.roomName);
	res.status = SUCCESS;

	auto newHandler = (IRoomRequestHandler*)_handlerFactory.createRoomAdminRequestHandler();
	newHandler->setCredentials(room, _user); // Function has to be called to set the data of the handler. can't be set from handler factory because function gets no parameters.
	requestRes.newHandler = (IRequestHandler*)newHandler;
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return requestRes;
}
