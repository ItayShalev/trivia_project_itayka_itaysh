#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include <set>

class RequestHandlerFactory;

class MenuRequestHandler : IRequestHandler
{
public:
	MenuRequestHandler(RequestHandlerFactory& requestHandlerFactorty, RoomManager& roomManager, StatisticsManager& statisticasManager, LoggedUser user);
	virtual bool isRequestRelevent(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo);


private:
	RequestResult logout(RequestInfo requestInfo);
	RequestResult getRooms(RequestInfo requestInfo);
	RequestResult getPlayersInRoom(RequestInfo requestInfo);
	RequestResult getStatistics(RequestInfo requestInfo);
	RequestResult joinRoom(RequestInfo requestInfo);
	RequestResult createRoom(RequestInfo requestInfo);


	LoggedUser _user;
	RoomManager& _roomManager;
	StatisticsManager& _statisticsManager;
	RequestHandlerFactory& _handlerFactory;
};