#pragma once

#include <string>

typedef struct Question
{
	int id;
	std::string question;
	std::string answer1;
	std::string answer2;
	std::string answer3;
	std::string answer4;
	int correctAnswer;
} Question;