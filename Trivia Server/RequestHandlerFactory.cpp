#include "RequestHandlerFactory.h"


RequestHandlerFactory::RequestHandlerFactory(LoginManager loginManager, IDatabase* database, StatisticsManager statisticsManager) : _loginManager(loginManager), _database(database), 
	_statisticsManager(statisticsManager)
{
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{

	return new LoginRequestHandler(*this, _loginManager);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return _loginManager;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler()
{
	return new MenuRequestHandler(*this, _roomManager, _statisticsManager, _loginManager.getLoggedUsers().back());
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler()
{
	return new RoomAdminRequestHandler(_roomManager, *this);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler()
{
	return new RoomMemberRequestHandler(_roomManager, *this);
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return _roomManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return this->_statisticsManager;
}
