#pragma once

#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomRequestHandlers.h"

#define SUCCESS 1
#define FALIURE 0

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(LoginManager loginManager, IDatabase* database, StatisticsManager statisticsManager);

	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler();
	RoomAdminRequestHandler* createRoomAdminRequestHandler();
	RoomMemberRequestHandler* createRoomMemberRequestHandler();

	LoginManager& getLoginManager();
	RoomManager& getRoomManager();
	StatisticsManager& getStatisticsManager();



private:
	LoginManager _loginManager;
	IDatabase* _database;
	RoomManager _roomManager;
	StatisticsManager _statisticsManager;
};