#pragma once

#include <string>
#include <ctime>
#include <vector>

enum class RequestCodes
{
	Login = 1,
	Signup,
	Logout,
	GetRooms,
	GetPlayersInRoom,
	JoinRoom,
	CreateRoom,
	GetStatistics,
	CloseRoom,
	StartGame,
	GetRoomState,
	LeaveRoom
};

// Login Handler

typedef struct LoginRequest
{
	std::string username;
	std::string password;
} LoginRequest;

typedef struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
} SignupRequest;

// Menu Handler

typedef struct GetPlayersInRoomRequest
{
	unsigned int roomid;
} GetPlayersInRoomRequest;

typedef struct JoinRoomRequest
{
	unsigned int roomid;
} JoinRoomRequest;

typedef struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
} CreateRoomRequest;

