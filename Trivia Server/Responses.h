#pragma once

#include <string>
#include <vector>
#include "Room.h"

enum class ResponseCodes
{
	error = 0,
	login,
	signup,
	logout,
	getRooms,
	getPlayersInRoom,
	joinRoom,
	createRoom,
	getStats, 
	closeRoom,
	startGame,
	getRoomState,
	leaveRoom
};


typedef struct ErrorResponse
{
	std::string message;
} ErrorResponse;

// Login Handler

typedef struct LoginResponse
{
	unsigned int status;
} LoginResponse;


typedef struct SignupResponse
{
	unsigned int status;
} SignupResponse;

// Menu Handler

typedef struct LogoutResponse
{
	unsigned int status;
} LogoutResponse;


typedef struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
} GetRoomsResponse;


typedef struct GetPlayersInRoomResponse
{
	unsigned int status;
	std::vector<std::string> players;
} GetPlayersInRoomResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;
} JoinRoomResponse;


typedef struct CreateRoomResponse
{
	unsigned int status;
} CreateRoomResponse;


typedef struct GetStatisticsResponse
{
	unsigned int status;
	json statistics;
} GetStatisticsResponse;

// Room Handlers

typedef struct CloseRoomResponse
{
	unsigned int status;
} CloseRoomResponse;

typedef struct StartGameResponse
{
	unsigned int status;
} StartGameResponse;

typedef struct GetRoomStateResponse
{
	unsigned int status;
	std::vector<std::string> players;
	unsigned int questionCount;
	float answerTimeout;
} GetRoomStateResponse;

typedef struct LeaveRoomResponse
{
	unsigned int status;
} LeaveRoomResponse;