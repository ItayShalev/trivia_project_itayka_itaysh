#include "Room.h"

Room::Room(LoggedUser admin)
{
	_metadata.id = 0;
	_metadata.roomState = State::open;
	_metadata.maxPlayers = 0;
	_metadata.timePerQuestion = 0.0;
	_users.push_back(admin);
}


void Room::addUser(LoggedUser user)
{
	_users.push_back(user);
}

void Room::removeUser(LoggedUser user)
{
	_users.erase(std::find(_users.begin(), _users.end(), user));
}

std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> users;
	for (auto user : _users)
	{
		users.push_back(user.getUsername());
	}
	return users;
}

RoomData Room::getMetadeta()
{
	return _metadata;
}

void Room::setMetadata(RoomData& other)
{
	_metadata = other;
}

void Room::setRoomState(State newState)
{
	_metadata.roomState = newState;
}

bool RoomData::operator==(RoomData& other)
{
	return this->id == other.id;
}
