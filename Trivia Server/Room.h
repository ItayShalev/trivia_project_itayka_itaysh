#pragma once
#include "LoggedUser.h"
#include "json.hpp"
#include <vector>

typedef nlohmann::json json;

enum class State
{
	closed = 0,
	open,
	began
};


typedef struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	float timePerQuestion;
	State roomState; 
	int questionCount;
	bool operator==(RoomData& other);

	//Both functions need to exist in order to create json out of it.
	friend void to_json(json& j, const RoomData& data)
	{
		j = json{ {"id", data.id}, {"name", data.name}, {"maxPlayers", data.maxPlayers},
			{"timePerQuestion", data.timePerQuestion}, {"roomState", data.roomState} };
	}
	friend void from_json(const json& j, RoomData& data)
	{
		j.at("id").get_to(data.id);
		j.at("name").get_to(data.name);
		j.at("maxPlayers").get_to(data.maxPlayers);
		j.at("timePerQuestion").get_to(data.timePerQuestion);
		j.at("roomState").get_to(data.roomState);
	}
} RoomData;


class Room
{
public:
	Room(LoggedUser admin);
	Room() = default;

	void addUser(LoggedUser user);
	void removeUser(LoggedUser user);

	// getters: 
	std::vector<std::string> getAllUsers();
	RoomData getMetadeta();

	// setters:
	void setMetadata(RoomData& other);
	void setRoomState(State newState);

private:
	RoomData _metadata;
	std::vector<LoggedUser> _users;

};