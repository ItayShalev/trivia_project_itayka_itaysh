#include "RoomManager.h"

Room RoomManager::createRoom(LoggedUser user, int maxPlayers, int questionTime, int questionCount, std::string roomName)
{
	int id = _rooms.size() + 1; // gets size of map and adds + 1 to get id.
	Room newRoom(user);
	RoomData data;
	data.id = id;
	data.roomState = State::open;
	data.maxPlayers = maxPlayers;
	data.timePerQuestion = questionTime;
	data.questionCount = questionCount;
	data.name = roomName;
	newRoom.setMetadata(data);
	_rooms.insert(std::make_pair(id, newRoom));
	return newRoom;
}

void RoomManager::deleteRoom(int ID)
{
	auto pos = _rooms.find(ID);
	if (pos != _rooms.end())
	{
		_rooms.erase(pos);
	}
	else
	{
		std::cerr << "Error: Room number not found" << std::endl;
	}
}

State RoomManager::getRoomState(int ID)
{
	if (_rooms.count(ID) > 0)
	{
		return _rooms[ID].getMetadeta().roomState;
	}
	std::cerr << "Error: Room number not found" << std::endl;
	return State::closed;
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> data;
	for (auto room : _rooms)
	{
		data.push_back(room.second.getMetadeta());
	}
	return data;
}

Room& RoomManager::getRoom(int id)
{
	auto it = _rooms.find(id);
	if (it != _rooms.end())
		return (*it).second;
	else
	{
		LoggedUser errorUser("Error");
		Room res(errorUser);
		return res;
	}
}
