#pragma once
#include "Room.h"
#include <map>
#include <iostream>

/* manages the rooms of the server and their data.*/
class RoomManager
{
public:
	Room createRoom(LoggedUser user, int maxPlayers, int questionTime, int questionCount, std::string roomName);
	void deleteRoom(int ID);
	State getRoomState(int ID);
	std::vector<RoomData>getRooms();
	Room& getRoom(int id);

private:
	std::map<int, Room> _rooms;
};
