#include "RoomRequestHandlers.h"

IRoomRequestHandler::IRoomRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory)
	: _roomManager(roomManager), _handlerFactory(requestHandlerFactory), _user(""), _room(*(new Room(LoggedUser(""))))
{
}

void IRoomRequestHandler::setCredentials(Room room, LoggedUser user)
{
	_room = room;
	_user = user;
}

RequestResult IRoomRequestHandler::getRoomState(RequestInfo)
{
	RequestResult requestRes;
	RoomData data = Room(_user).getMetadeta();
	data.roomState = State::closed;
	try
	{
		_room = _roomManager.getRoom(_room.getMetadeta().id);
		data = _room.getMetadeta();
	}
	catch (...)
	{
	}
	// If the room's closed or is beginning, then a different message should be sent (ususally that'd happen to the member not the admin, because the admin is the one
	// Who sets the room to these states).
	// In all other cases a normel roomState response is sent.
	// Regardless, a getRoomState request is sent every 3 seconds by every member, including the admin, of the room.
	if (data.roomState == State::began)
	{
		StartGameResponse res;
		res.status = (int)data.roomState;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
		requestRes.newHandler = (IRequestHandler*)this; // TODO: this is a placeholder for the future
	}
	else if (data.roomState == State::closed)
	{
		CloseRoomResponse res;
		res.status = (int)data.roomState;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
		_handlerFactory.getLoginManager().refresh(_user.getUsername());
		requestRes.newHandler = (IRequestHandler*)_handlerFactory.createMenuRequestHandler();
	}
	else
	{
		GetRoomStateResponse res;
		res.status = (int)data.roomState;
		res.players = _room.getAllUsers();
		res.questionCount = data.questionCount;
		res.answerTimeout = data.timePerQuestion;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
		requestRes.newHandler = (IRequestHandler*)this; 
	}
	return requestRes;
}


RoomAdminRequestHandler::RoomAdminRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory)
	: IRoomRequestHandler(roomManager, requestHandlerFactory) // calls the constructor of base class
{
}

bool RoomAdminRequestHandler::isRequestRelevent(RequestInfo requestInfo)
{
	std::set<RequestCodes> validCodes = { RequestCodes::CloseRoom, RequestCodes::GetRoomState, RequestCodes::StartGame };// A set of all the valid codes that can be used in the menu.
	return validCodes.find((RequestCodes)requestInfo.id) != validCodes.end(); // Uses the 'find' function, returns the past-the-end iterator if element not found.

}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestRes;
	try
	{
		switch (requestInfo.id)
		{
		case (int)RequestCodes::CloseRoom:
			requestRes = closeRoom(requestInfo);
			break;
		case (int)RequestCodes::GetRoomState:
			requestRes = getRoomState(requestInfo);
			break;
		case (int)RequestCodes::StartGame:
			requestRes = startGame(requestInfo);
			break;
		}
	}
	catch (std::exception& exception)
	{
		ErrorResponse res;
		std::cerr << "admin room handler error: " << exception.what() << std::endl;
		res.message = std::string("admin room handler error: ") + exception.what();
		requestRes.newHandler = nullptr;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	}
	return requestRes;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo requestInfo)
{
	RequestResult requestRes;
	CloseRoomResponse res;
	_room.setRoomState(State::closed);
	// The fucntion can fail, and if it does it throws an error.
	// In that case set the status to faliure.
	try
	{
		_roomManager.deleteRoom(_room.getMetadeta().id);
	}
	catch (std::exception)
	{
		res.status = FALIURE;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
		requestRes.newHandler = (IRequestHandler*)this;
		return requestRes;
	}
	res.status = SUCCESS;
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	_handlerFactory.getLoginManager().refresh(_user.getUsername());
	requestRes.newHandler = (IRequestHandler*)_handlerFactory.createMenuRequestHandler();
	return requestRes;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo requestInfo)
{
	RequestResult requestRes;
	StartGameResponse res;
	_room.setRoomState(State::began);
	res.status = SUCCESS;
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	requestRes.newHandler = (IRequestHandler*)this; //TODO: this is a placeholder for the future
	return requestRes;
}

RoomMemberRequestHandler::RoomMemberRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory)
	: IRoomRequestHandler(roomManager, requestHandlerFactory) // calls the constructor of base class
{
}

bool RoomMemberRequestHandler::isRequestRelevent(RequestInfo requestInfo)
{
	std::set<RequestCodes> validCodes = { RequestCodes::LeaveRoom, RequestCodes::GetRoomState };// A set of all the valid codes that can be used in the menu.
	return validCodes.find((RequestCodes)requestInfo.id) != validCodes.end(); // Uses the 'find' function, returns the past-the-end iterator if element not found.
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestRes;
	try
	{
		//Only 2 possible options exist, because I know that the request is either getRoomState or leavGame.
		requestRes = (requestInfo.id == (int)RequestCodes::LeaveRoom) ? leaveGame(requestInfo) : getRoomState(requestInfo); 
	}
	catch (std::exception exception)
	{
		ErrorResponse res;
		std::cerr << "member room handler error" << exception.what() << std::endl;
		res.message = std::string("member room handler error") + exception.what();
		requestRes.newHandler = nullptr;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	}
	return requestRes;
}

RequestResult RoomMemberRequestHandler::leaveGame(RequestInfo requestInfo)
{
	RequestResult requestRes;
	LeaveRoomResponse res;
	// The fucntion can fail, and if it does it throws an error.
	// In that case set the status to faliure.
	try
	{
		_roomManager.getRoom(_room.getMetadeta().id).removeUser(_user);
	}
	catch (std::exception)
	{
		res.status = FALIURE;
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
		requestRes.newHandler = (IRequestHandler*)this;
		return requestRes;
	}
	res.status = SUCCESS;
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	_handlerFactory.getLoginManager().refresh(_user.getUsername());
	requestRes.newHandler = (IRequestHandler*)_handlerFactory.createMenuRequestHandler();
	return requestRes;
}
