#pragma once
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

/* Base class for the two different room handler - the admin and the member handlers.
 Base class exists because some elements exist in both the admin and member handlers, for example the getRoomState function can be the same for both handlers, 
 and the members of both classes are the same.
*/

class IRoomRequestHandler : public IRequestHandler
{
public:
	IRoomRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory);
	virtual bool isRequestRelevent(RequestInfo requestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo requestInfo) = 0;
	// Sets the user and the room of the handler. must be called after constructor.
	void setCredentials(Room room, LoggedUser user); 

protected:
	RequestResult getRoomState(RequestInfo requestInfo);

	Room& _room;
	LoggedUser _user;
	RoomManager& _roomManager;
	RequestHandlerFactory& _handlerFactory;
};



class RoomAdminRequestHandler : IRoomRequestHandler
{
public:
	RoomAdminRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory);
	bool isRequestRelevent(RequestInfo requestInfo);
	RequestResult handleRequest(RequestInfo requestInfo);

private:
	RequestResult closeRoom(RequestInfo requestInfo);
	RequestResult startGame(RequestInfo requestInfo);
};



class RoomMemberRequestHandler : IRoomRequestHandler
{
public:
	RoomMemberRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory);
	virtual bool isRequestRelevent(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo);

private:
	RequestResult leaveGame(RequestInfo requestInfo);
};