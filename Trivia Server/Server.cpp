#pragma comment (lib, "ws2_32.lib")

#include "Server.h"

// This constructor calls to the second private constructor with a new dataBase, because the database must exist for proper initialization.
Server::Server() : Server{new SqliteDatabase()}
{
}

Server::Server(IDatabase* database) : m_database(database), m_handlerFactory(LoginManager(database), database, StatisticsManager(database)), _communicator(this->m_handlerFactory)
{
}

Server::~Server()
{
	delete this->m_database;
}

void Server::run()
{
	std::string command = "";
	while (command != "EXIT")
	{
		try
		{
			WSAInitializer WSAInit;
			this->_communicator.startHandleRequests();
			while (command != "EXIT")
			{
				std::cin >> command;
			}
		}
		catch (std::exception & e)
		{
			std::cout << e.what() << std::endl;
		}
	}
}