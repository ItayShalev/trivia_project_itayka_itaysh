#pragma once

#include "Communicator.h"
#include "WSAInitializer.h"
#include "RequestHandlerFactory.h"
#include "SqliteDatabase.h"

class Server
{
public:
	Server();
	~Server();

	void run();

private:
	Server(IDatabase* database);

	Communicator _communicator;
	RequestHandlerFactory m_handlerFactory;	
	IDatabase* m_database;
};