#include "SqliteDatabase.h"

std::vector<User> SqliteDatabase::_userResults;
std::list<Question> SqliteDatabase::_questionResults;
Statistics SqliteDatabase::_statsResult;

SqliteDatabase::SqliteDatabase()
{
	bool isFileCreated = _access(DATABASE_PATH, CHECK_FOR_FILE) == FILE_NOT_FOUND; // Checking if the file doesn't exist

	if (!sqlite3_open(DATABASE_PATH, &(this->_database)) == SQLITE_OK) // Opens (or creates and opens) the database and returns if it was opened
	{
		this->_database = nullptr;
		throw std::exception("Failed to open the database"); // Notifies the user
	}

	if (isFileCreated) // If the file wasn't already created
	{
		// Creates the tables.
		executeCommand("CREATE TABLE Users (Username TEXT NOT NULL PRIMARY KEY, Password TEXT NOT NULL, Email TEXT NOT NULL);");
		executeCommand("CREATE TABLE Questions (Id INTEGER PRIMARY KEY AUTOINCREMENT, Question TEXT NOT NULL, Answer1 TEXT NOT NULL, Answer2 TEXT NOT NULL, Answer3 TEXT NOT NULL, Answer4 TEXT NOT NULL, CorrectAnswer INTEGER NOT NULL);");
		executeCommand("CREATE TABLE Statistics (Username TEXT NOT NULL PRIMARY KEY, GamesPlayed INTEGER NOT NULL, CorrectAnswers INTEGER NOT NULL, TotalAnswers INTEGER NOT NULL, TotalAnswerTime REAL NOT NULL, Score INTEGER NOT NULL);");

		// Insert questions into the database
		std::cout << "Enter a path to a file with question generated from \"https://opentdb.com/api.php?amount=10&category=18&difficulty=easy&type=multiple\": ";
		std::string path;
		std::cin >> path;
		initializeQuestions(path);
	}
}

SqliteDatabase::~SqliteDatabase()
{
	sqlite3_close(this->_database); // Closes the database
	this->_database = nullptr; // Fixes memory leaks
}

bool SqliteDatabase::doesUserExists(std::string username)
{
	bool exists = false;

	// Selects all users with the same name as the username
	selectQuery(userCallback, "Username", "Users", "Username == \"" + username + "\"");

	// Validates that no one was found
	if (!_userResults.empty())
	{
		exists = true;
	}

	// Clears the result
	_userResults.clear();

	return exists; // Returns if the user exists
}

bool SqliteDatabase::doesPasswordMatch(std::string password, std::string username)
{
	bool matches = false;

	// Selects all users with the same username
	selectQuery(userCallback, "*", "Users", "Username == \"" + username + "\"");

	if (_userResults.size() == 0)
	{
		return false;
	}

	// Checks if the password validates
	matches = _userResults[0].password == password;

	// Clears the result
	_userResults.clear();

	return matches; // Returns if the password matches
}

void SqliteDatabase::addNewUser(std::string email, std::string username, std::string password)
{
	// Inserts a new user into the database using the INSERT command
	executeCommand("INSERT INTO Users (Email, Username, Password) VALUES (\"" + email + "\", \"" + username + "\", \"" + password + "\");");
}

std::list<Question> SqliteDatabase::getQuestions(int count)
{
	// Clear the result
	_questionResults.clear();

	// Get <count> questions
	selectQuery(questionCallback, "*", "Questions LIMIT " + std::to_string(count));

	// If there are not enough questions
	if (_questionResults.size() < count)
	{
		throw std::exception("No questions available.");
	}

	return _questionResults;
}

float SqliteDatabase::getAverageAnswerTime(std::string username)
{
	selectQuery(statisticsCallback, "*", "Statistics", "WHERE Username = \"" + username + "\"");

	return _statsResult.totalAnswerTime / _statsResult.totalAnswers;
}

int SqliteDatabase::getNumOfCorrectAnswers(std::string username)
{
	selectQuery(statisticsCallback, "*", "Statistics", "WHERE Username = \"" + username + "\"");

	return _statsResult.correctAnswers;
}

int SqliteDatabase::getNumOfTotalAnswers(std::string username)
{
	selectQuery(statisticsCallback, "*", "Statistics", "WHERE Username = \"" + username + "\"");

	return _statsResult.totalAnswers;
}

int SqliteDatabase::getNumOfPlayerGames(std::string username)
{
	selectQuery(statisticsCallback, "*", "Statistics", "WHERE Username = \"" + username + "\"");

	return _statsResult.gamesPlayed;
}

Statistics SqliteDatabase::getAllStatistics(std::string username)
{
	selectQuery(statisticsCallback, "*", "Statistics", "WHERE Username = \"" + username + "\"");

	return _statsResult;
}

std::vector<Statistics> SqliteDatabase::getHighestPlayersStatistics()
{
	int i = 0;
	std::vector<Statistics> stats;
	for (i = 0; i < 5; i++)
	{
		selectQuery(statisticsCallback, "*", "Statistics", "LIMIT 1 OFFSET \"" + std::to_string(i) + "\"");
		stats.push_back(_statsResult);
	}
	return stats;
}


void SqliteDatabase::initializeQuestions(std::string path)
{
	std::ifstream file(path); // Open file
	std::stringstream fileDataStream; // Create string stream
	fileDataStream << file.rdbuf(); // Convert file into string stream
	std::string fileData = fileDataStream.str(); // Convert string stream into a string
	
	json questions = json::parse(fileData)["results"]; // Convert the string to json and get the questions
	for (auto question : questions)
	{
		// Insert each question into the table
		executeCommand("INSERT INTO Questions (Question, Answer1, Answer2, Answer3, Answer4, CorrectAnswer) VALUES (\"" + std::string(question["question"]) + "\", \"" + std::string(question["correct_answer"]) + "\", \"" + std::string(question["incorrect_answers"][0]) + "\", \"" + std::string(question["incorrect_answers"][1]) + "\", \"" + std::string(question["incorrect_answers"][2]) + "\", 1)");
	}
}

void SqliteDatabase::selectQuery(int(*callback)(void*, int, char**, char**), std::string items, std::string table, std::string condition)
{
	char* errMessage = nullptr; // Initializes the error message c string
	// Generates the command, with WHERE clause as an option
	std::string command = "SELECT " + items + " FROM " + table + (condition != "" ? " WHERE " + condition : "") + ";";
	int queryResult = sqlite3_exec(this->_database, command.c_str(), callback, nullptr, &errMessage); // Executes the command
	if (queryResult != SQLITE_OK) // If something went wrong
	{
		throw std::exception(errMessage); // Notifies the user what went wrong
	}
}

void SqliteDatabase::executeCommand(std::string command)
{
	char* errMessage = nullptr; // Initializes the error message c string
	// Executes the given command
	int queryResult = sqlite3_exec(this->_database, command.c_str(), nullptr, nullptr, &errMessage);
	if (queryResult != SQLITE_OK) // If something went wrong
	{
		throw std::exception(errMessage); // Notifies the user what went wrong
	}
}

int SqliteDatabase::userCallback(void* data, int argc, char** argv, char** azColName)
{
	// Creates variables for the fields
	int i = 0;
	User user;
	// For each column it matches a corresponding variable
	for (i = 0; i < argc; i++)
	{
		if (azColName[i] == std::string("Username"))
		{
			user.username = argv[i];
		}
		else if (azColName[i] == std::string("Password"))
		{
			user.password = argv[i];
		}
		else if (azColName[i] == std::string("Email"))
		{
			user.email = argv[i];
		}
	}
	_userResults.push_back(user); // Adds the item to the result
	return 0;
}

int SqliteDatabase::questionCallback(void* data, int argc, char** argv, char** azColName)
{
	// Creates variables for the fields
	int i = 0;
	Question question;
	// For each column it matches a corresponding variable
	for (i = 0; i < argc; i++)
	{
		if (azColName[i] == std::string("Question"))
		{
			question.question = argv[i];
		}
		else if (azColName[i] == std::string("Answer1"))
		{
			question.answer1 = argv[i];
		}
		else if (azColName[i] == std::string("Answer2"))
		{
			question.answer2 = argv[i];
		}
		else if (azColName[i] == std::string("Answer3"))
		{
			question.answer3 = argv[i];
		}
		else if (azColName[i] == std::string("Answer4"))
		{
			question.answer4 = argv[i];
		}
		else if (azColName[i] == std::string("CorrectAnswer"))
		{
			question.correctAnswer = std::atoi(argv[i]);
		}
		else if (azColName[i] == std::string("Id"))
		{
			question.id = std::atoi(argv[i]);
		}
	}
	_questionResults.push_back(question); // Adds the item to the result
	return 0;
}

int SqliteDatabase::statisticsCallback(void* data, int argc, char** argv, char** azColName)
{
	// Creates variables for the fields
	int i = 0;
	Statistics stats;
	// For each column it matches a corresponding variable
	for (i = 0; i < argc; i++)
	{
		if (azColName[i] == std::string("GamesPlayed"))
		{
			stats.gamesPlayed = std::atoi(argv[i]);
		}
		else if (azColName[i] == std::string("CorrectAnswers"))
		{
			stats.correctAnswers = std::atoi(argv[i]);
		}
		else if (azColName[i] == std::string("TotalAnswers"))
		{
			stats.totalAnswers = std::atoi(argv[i]);
		}
		else if (azColName[i] == std::string("TotalAnswerTime"))
		{
			stats.totalAnswerTime = std::atof(argv[i]);
		}
		else if (azColName[i] == std::string("Username"))
		{
			stats.username = argv[i];
		}
	}
	_statsResult = stats;
	return 0;
}
