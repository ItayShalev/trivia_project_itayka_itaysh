#pragma once

#include "sqlite3.h"
#include "IDatabase.h"
#include "User.h"
#include "json.hpp"

#include <io.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#define DATABASE_PATH "TriviaDatabase.sqlite"
#define CHECK_FOR_FILE 0
#define FILE_NOT_FOUND -1

typedef nlohmann::json json;

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	~SqliteDatabase();

	virtual bool doesUserExists(std::string username);
	virtual bool doesPasswordMatch(std::string password, std::string username);
	virtual void addNewUser(std::string email, std::string username, std::string password);

	virtual std::list<Question> getQuestions(int count);

	virtual float getAverageAnswerTime(std::string username);
	virtual int getNumOfCorrectAnswers(std::string username);
	virtual int getNumOfTotalAnswers(std::string username);
	virtual int getNumOfPlayerGames(std::string username);
	virtual Statistics getAllStatistics(std::string username);
	virtual std::vector<Statistics> getHighestPlayersStatistics();

private:
	sqlite3* _database;

	void initializeQuestions(std::string path);

	void selectQuery(int(*callback)(void*, int, char**, char**), std::string items, std::string table, std::string condition = "");
	void executeCommand(std::string command);

	static std::vector<User> _userResults;
	static std::list<Question> _questionResults;
	static Statistics _statsResult;

	static int userCallback(void* data, int argc, char** argv, char** azColName);
	static int questionCallback(void* data, int argc, char** argv, char** azColName);
	static int statisticsCallback(void* data, int argc, char** argv, char** azColName);
};

