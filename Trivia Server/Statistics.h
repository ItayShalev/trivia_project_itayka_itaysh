#pragma once

typedef struct Statistics
{
	std::string username;
	int correctAnswers;
	int totalAnswers;
	int gamesPlayed;
	float totalAnswerTime;
} Statistics;