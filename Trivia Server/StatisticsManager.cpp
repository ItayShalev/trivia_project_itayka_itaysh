#include "StatisticsManager.h"

StatisticsManager::StatisticsManager(IDatabase* database) : _database(database)
{
	// Remains empty because of initialization list
}

json StatisticsManager::getStatistics(std::string username)
{
	json jsonStats;
	if (!_database->doesUserExists(username))
	{
		return jsonStats;
	}
	std::vector<Statistics> highestOalyersStats = _database->getHighestPlayersStatistics();
	Statistics stats = _database->getAllStatistics(username);
	jsonStats["correctAnswers"] = stats.correctAnswers;
	jsonStats["totalAnswers"] = stats.totalAnswers;
	jsonStats["gamesPlayed"] = stats.gamesPlayed;
	jsonStats["totalAnswerTime"] = stats.totalAnswerTime;

	
	for (auto userStats : highestOalyersStats)
	{
		jsonStats["topUser"][userStats.username]["correctAnswers"] = userStats.correctAnswers;
		jsonStats["topUser"][userStats.username]["totalAnswers"] = userStats.totalAnswers;
		jsonStats["topUser"][userStats.username]["gamesPlayed"] = userStats.gamesPlayed;
		jsonStats["topUser"][userStats.username]["totalAnswerTime"] = userStats.totalAnswerTime;
	}
	
	return jsonStats;
}
