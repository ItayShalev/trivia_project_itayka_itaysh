#pragma once
#include "SqliteDatabase.h"


typedef nlohmann::json json;

class StatisticsManager
{
public:
	StatisticsManager(IDatabase* database);
	json getStatistics(std::string username);


private:
	IDatabase* _database;
};