#pragma once

#include <string>

typedef struct User
{
	std::string username;
	std::string email;
	std::string password;
} User;