import socket
import ipaddress
import msgpack
from collections import deque
from time import sleep

inbound = deque("")
outbound = deque(["hello"])


def setup():
    """
    Asks the user for basic information about the socket - the address and port.
    :return:
    """
    valid = False
    while not valid:
        address = input("Enter server IP address: ")
        try:
            ipaddress.ip_address(address)
        except ValueError:
            print("Error: Invalid server IP address, pleas try again.")
            continue
        try:
            port = int(input("Enter server port (Valid numbers are 1024 - 65535: "))
            if port < 1024 or port > 65535:
                raise ValueError
        except ValueError:
            print("Error: Invalid server port, please try again.")
            continue
        valid = True
    return address, port


def send_outbound(sock, amount=-1):
    """
    Sends messages from the outbound deque, removing them in the process.
    can send multiple messages one after another according to the amount parameter.
    :param sock: The server conversation socket.
    :param amount: The amount of messages to send. Defaults to -1 which means all messages. If amount is longer than
        the deque size, sends all messages in deque.
    :return: None.
    """
    if amount == -1 or amount > len(outbound):
        amount = len(outbound)

    for i in range(amount):
        msg = (outbound.popleft())
        sock.sendall(msg.encode())
        print("Sent message is: " + msg)
        sleep(0.5)


def add_msg(sock, msg, sendall=False):
    """
    Adds a new message to the outbound deque, to be sent later.
    sendall can be passed as true to send all messages in the deque including the new message.
    :param sock: The server conversation socket
    :param msg: The massege that will be added to the deque.
    :param sendall: can be passed as True to send all messages in deque, defaults to False
    :return: None.
    """
    outbound.append(msg)
    if sendall:
        send_outbound(sock)


def receive_msg(sock):
    """
    A function that blocks the program's thread and waits for the server to send a response to the client.
    After a response has been received, adds it to the inbound deque (in case this function is running in a thread.
    :param sock: The server conversation socket.
    :return: None.
    """
    data = sock.recv(2024).decode()
    inbound.append(data)


def login_msg(sock):
    """
    Asks the user for a username and password to send to the server as a login message.
    Serializes and sends the login message with the code '1', along with all the other messages in the queue.
    :param sock: The server conversation socket.
    :return: None.
    """
    username = input("Enter username: ")
    password = input("Enter password: ")
    json_msg = msgpack.packb({"username": username, "password": password})
    bytes_msg = bytes([1]) + bytearray(len(json_msg).to_bytes(4, 'big')) + bytes(json_msg, 'utf-8')
    add_msg(sock, bytes_msg, True)


def signup_msg(sock):
    """
    Asks the user for a username, password and email to send to the server as a signup message.
    Serializes and sends the login message with the code '2', along with all the other messages in the queue.
    :param sock: The server conversation socket.
    :return: None.
    """
    username = input("Enter username: ")
    password = input("Enter password: ")
    email = input("Enter email: ")
    json_msg = msgpack.packb({"username": username, "password": password, "email": email})
    bytes_msg = bytes([2]) + bytearray(len(json_msg).to_bytes(4, 'big')) + bytes(json_msg, 'utf-8')
    add_msg(sock, bytes_msg, True)


def main():
    address = setup()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect(address)
        while True:
            choice = input("Choose message type: 1 - login, 2 - signup")
            login_msg(sock) if choice == 1 else signup_msg(sock)
            receive_msg(sock)
            print("Received message is: " + inbound.popleft())


if __name__ == '__main__':
    main()
