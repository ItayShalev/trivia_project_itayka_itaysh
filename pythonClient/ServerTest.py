import socket
from time import sleep

HOST = "127.0.0.1"
PORT = 2212

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    print("listening...")
    s.listen()
    conn, addr = s.accept()
    print("connected")
    with conn:
        print('Connected by', addr)
        while True:
            data = conn.recv(2024)
            conn.sendall(data)
            print("Data received and sent: " + data.decode())
            sleep(0.5)
