import json
import socket
import math
import sys
import os

##############
### Consts ###
##############

FIRST_ARG_IDX = 1

# Default Message Codes
# LOGIN_REQUEST_ID = (1).to_bytes(1, byteorder='big')
# SIGNUP_REQUEST_ID = (2).to_bytes(1, byteorder='big')
# GET_PLAYERS_REQUEST = (3).to_bytes(1, byteorder='big')
# JOIN_ROOM_REQUEST = (4).to_bytes(1, byteorder='big')
# CREATE_ROOM_REQUEST = (5).to_bytes(1, byteorder='big')
# SIGNOUT_REQUEST = (6).to_bytes(1, byteorder='big')
# GET_ROOMS_REQUEST = (7).to_bytes(1, byteorder='big')
# GET_HISCORES_REQUEST = (8).to_bytes(1, byteorder='big')
# ERROR_RESPONSE_ID = (100).to_bytes(1, byteorder='big')

# Packet Menu Options
LOGIN = 1
SIGNUP = 2
GET_ROOMS = 3
JOIN_ROOM = 4
GET_PLAYERS = 5
CREATE_ROOM = 6
GET_HISCORES = 7
SIGNOUT = 8

# Test Menu Options
V101 = 1
V102 = 2
Vother = 3


#######################
### Message Related ###
#######################


def send(sock, dict={}):
    send_from = 0
    string_dic = ""
    if dict is not {}:
        string_dic = json.dumps(dict)
    print("Enter the message code (1 Byte): ")
    id = int(input()).to_bytes(1, byteorder='big')
    message = id + len(string_dic).to_bytes(4, byteorder='big') + string_dic.encode("ASCII")
    while send_from < len(message):
        send_from += sock.send(message[send_from:])


def recv(sock):
    id = sock.recv(1)
    length = int.from_bytes(sock.recv(4), byteorder='big')
    message = ""
    while len(message) < length:
        message += (sock.recv(length)).decode("ASCII")
    return message


def hello(sock):
    send_from = 0
    sock.send(b'Hello')
    message = sock.recv(5).decode("ASCII")
    if message == 'Hello':
        cprint("Hello message received successfully", "green", "black")
    else:
        cprint("Received wrong message, expected 'Hello' but received '" + str(message) + "'", "red", "black")
    exit()


def json_packet(sock):
    dict_ = {"key1": "val1",
             "key2": "val2"}
    send(sock, dict_)
    message = str(recv(sock))
    if message == '{"response_code": "10"}':
        cprint("message received successfully with response code 10", "green", "black")
    else:
        cprint("Received wrong json, expected '{\"response_code\": \"10\"}' but received '" + str(message) + "'", "red",
               "black")
    exit()


def signup(sock):
    ##### Mandatory user attributes ####
    username = input("Enter username: ")
    password = input("Enter password: ")
    email = input("Enter email: ")

    ##### Bonus attributes for V1.0.0 (not mandatory) #####
    # birth = input("Enter birthdate (dd/MM/yyyy): ")
    # phone = input("Enter phone number: ")
    # address = input("Enter address: ")

    dic = {"username": username,
           "password": password,
           "email": email,
           #    "birthdate": birth,
           #    "phone": phone,
           #    "address": address
           }

    send(sock, dic)


def login(sock):
    username = input("Enter username: ")
    password = input("Enter password: ")
    dic = {"username": username,
           "password": password}
    send(sock, dic)


def get_rooms(sock):
    send(sock)


def print_rooms(sock):
    rooms = get_rooms(sock)
    i = 1
    for dic in rooms:
        print(str(i) + " " + dic["admin"] + "'s room")
        i += 1
    room = int(input("Enter room id: "))


def join_room(sock):
    room = print_rooms(sock)
    dict = {"id": room["id"]}
    send(sock, dict)


def get_players_in_room(sock):
    room = print_rooms(sock)
    dict = {"id": room["id"]}
    send(sock, dict)


def create_room(sock):
    username = input("What's your username? ")
    maxUsers = int(input("What is the maximal number of users? "))
    questionCount = int(input("How many questions? "))
    answerTimeout = int(input("How many seconds per question?"))
    dic = {"username": username, "maxUsers": maxUsers, "questionCount": questionCount, "answerTimeout": answerTimeout}
    send(sock, dic)


def get_highscores(sock):
    send(sock)


def signout(sock):
    send(sock)


#######################
### Display Related ###
#######################

# copied from https://pypi.org/project/colorama/
from colorama import init, Fore, Back, Style

init()


def cprint(msg, foreground="black", background="white"):
    fground = foreground.upper()
    bground = background.upper()
    style = getattr(Fore, fground) + getattr(Back, bground)
    print(style + msg + Style.RESET_ALL)


#####################
### Error Related ###
#####################

def print_error_message(err_msg):
    cprint(err_msg, "red", "black")
    os.system('pause')


def connection_error_message():
    print_error_message("Could not connect to the server,\nplease check if the server is accepting connections")


def wrong_option_error():
    print_error_message("wrong choice, please select a valid option")


def port_error_message():
    print_error_message("wrong port number, port number should be 1024-65535")


####################
### Menu Related ###
####################

def intro_screen():
    print("\n"
          + " ____  ____  ____  _  _  ____    __      ____  ____  ___  ____  ____  ____  \n"
          + "(_  _)(  _ \(_  _)( \/ )(_  _)  /__\    (_  _)( ___)/ __)(_  _)( ___)(  _ \ \n"
          + "  )(   )   / _)(_  \  /  _)(_  /(__)\     )(   )__) \__ \  )(   )__)  )   / \n"
          + " (__) (_)\_)(____)  \/  (____)(__)(__)   (__) (____)(___/ (__) (____)(_)\_) \n")


def print_select_test_menu():
    cprint("Please choose what to send: \n", "black", "white")
    cprint("[1] --- V1.0.1 - basic hello message ", "yellow", "black")
    cprint("[2] --- V1.0.2 - json format message ", "magenta", "black")
    cprint("[3] --- other request messages ", "green", "black")


def print_select_packet_menu():
    cprint("Please choose a request message to send: \n", "black", "white")
    cprint("[1] --- login request ", "yellow", "black")
    cprint("[2] --- signup request ", "magenta", "black")
    cprint("[3] --- get rooms request", "blue", "black")
    cprint("[4] --- join room request ", "green", "black")
    cprint("[5] --- get players in room request ", "cyan", "black")
    cprint("[6] --- create room request ", "white", "black")
    cprint("[7] --- get highscores request ", "yellow", "black")
    cprint("[8] --- signout request ", "magenta", "black")


def send_packet(option, client_socket):
    if option == LOGIN:
        login(client_socket)
    elif option == SIGNUP:
        signup(client_socket)
    elif get_rooms == GET_ROOMS:
        get_rooms(client_socket)
    elif option == JOIN_ROOM:
        join_room(client_socket)
    elif option == GET_PLAYERS:
        get_players_in_room(client_socket)
    elif option == CREATE_ROOM:
        create_room(client_socket)
    elif option == GET_HISCORES:
        get_highscores(client_socket)
    elif signout == SIGNOUT:
        login(client_socket)
    else:
        raise TypeError


try:
    os.system('cls')
    intro_screen()

    # get port number
    port = input("Enter the server port number: ")
    if int(port) < 1024 or int(port) > 65535:
        raise ValueError
    print("\ntrying to connect...\n")

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('localhost', int(port)))

    # connection established
    print_select_test_menu()
    choice = int(input())
    if (choice == V101):
        hello(client_socket)
    elif (choice == V102):
        json_packet(client_socket)
    elif (choice == Vother):
        os.system('cls')
        intro_screen()
        print_select_packet_menu()
        choice = int(input())
        send_packet(choice, client_socket)
    else:
        raise TypeError

    print("\nPacket sent. Waiting for server response...\n")

    # message sent, waiting for server's response
    message = str(recv(client_socket))
    cprint("Received response from server:\n" + message, "yellow", "black")

except TypeError:
    wrong_option_error()

except ValueError:
    port_error_message()

except ConnectionRefusedError:
    connection_error_message()
